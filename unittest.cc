
/**
 * @file unittest.cc
 * @author Uwe Koecher (UK)
 * @date 2018-06-12, UK
 *
 * @brief unit test for dG make flux sparsity with Q-Gauss and Q-Gauss-Lobatto
 */

////////////////////////////////////////////////////////////////////////////////
#define GaussLobatto

////////////////////////////////////////////////////////////////////////////////
// #define DoFRenumbering_component_wise
////////////////////////////////////////////////////////////////////////////////

/*  Copyright (C) 2012-2018 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.   If not, see <http://www.gnu.org/licenses/>.           */

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/utilities.h>

#include <deal.II/base/mpi.h>
#include <deal.II/base/multithread_info.h>
#include <deal.II/base/table.h>
#include <deal.II/base/numbers.h>

#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/index_set.h>

#include <deal.II/distributed/tria.h>
#include <deal.II/distributed/grid_refinement.h>

#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/fe_dgq.h>

#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>

#include <deal.II/lac/trilinos_sparsity_pattern.h>
#include <deal.II/lac/block_sparsity_pattern.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
#define REFINE_GLOBAL 1
#define FE_P 1

#define MPIX_THREADS 1

////////////////////////////////////////////////////////////////////////////////
template<int dim>
class UnitTest
{
public:
	UnitTest () :
		mpi_comm(MPI_COMM_WORLD),
		pcout(
			std::cout,
			!dealii::Utilities::MPI::this_mpi_process(mpi_comm)
		)
	{};
	
	virtual ~UnitTest() {
		if (dof.use_count()) {
			dof->clear();
		}
	}
	
	void run () {
		make_grid();
		setup_fe();
		distribute();
		
		make_partitioning();
		
		make_sparsity_pattern();
	};
	
private:
	void make_grid ();
	void setup_fe ();
	void distribute ();
	
	void make_partitioning ();
	
	void make_sparsity_pattern ();
	
	MPI_Comm mpi_comm;
	dealii::ConditionalOStream pcout;
	
	std::shared_ptr< dealii::parallel::distributed::Triangulation<dim> > tria;
	std::shared_ptr< dealii::DoFHandler<dim> > dof;
	std::shared_ptr< dealii::FESystem<dim> > fe;
	
	std::shared_ptr< dealii::IndexSet > locally_owned_dofs;
	std::shared_ptr< dealii::IndexSet > locally_relevant_dofs;
	
	std::shared_ptr< std::vector< dealii::IndexSet > > partitioning_locally_owned_dofs;
	std::shared_ptr< std::vector< dealii::IndexSet > > partitioning_locally_relevant_dofs;
	
	std::shared_ptr< dealii::TrilinosWrappers::SparsityPattern > sp_A;
};


////////////////////////////////////////////////////////////////////////////////
template<int dim>
void UnitTest<dim>::
make_grid ()
{
	tria = std::make_shared< dealii::parallel::distributed::Triangulation<dim> >(
		mpi_comm,
		typename dealii::Triangulation<dim>::MeshSmoothing(
			dealii::Triangulation<dim>::smoothing_on_refinement |
			dealii::Triangulation<dim>::smoothing_on_coarsening
		)
	);
	
	dof = std::make_shared< dealii::DoFHandler<dim> >(*tria);
	
	dealii::GridGenerator::hyper_cube(
		*tria,
		0.0, 1.0
	);
	
	tria->refine_global(REFINE_GLOBAL);
	
	pcout
		<< "Number of active cells: "
		<< tria->n_active_cells()
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
setup_fe ()
{
	// quadrature to create FE_DGQAr
	std::shared_ptr< dealii::Quadrature<1> > fe_quad;
	
#ifdef GaussLobatto
	// problematic for flux entries (some are missing)
	fe_quad = std::make_shared< dealii::QGaussLobatto<1> > (FE_P+1);
#else
	fe_quad = std::make_shared< dealii::QGauss<1> > (FE_P+1);
#endif
	
// 	fe = std::make_shared< dealii::FESystem<dim> > (
// 		// mechanics FE (component 0 ... dim-1)
// 		dealii::FESystem<dim> (
// 			dealii::FE_DGQ<dim>(*fe_quad), dim
// 		), 1
// 	);
	
	fe = std::make_shared< dealii::FESystem<dim> > (
		// mechanics FE (component 0 ... dim-1)
		dealii::FESystem<dim> (
			dealii::FE_DGQArbitraryNodes<dim>(*fe_quad), dim
		), 1
	);
	
	pcout
		<< "ewave_dG: created " << fe->get_name()
		<< std::endl;
}


template<int dim>
void UnitTest<dim>::
distribute ()
{
	AssertThrow(fe.use_count(), dealii::ExcNotInitialized());
	AssertThrow(dof.use_count(), dealii::ExcNotInitialized());
	dof->distribute_dofs(*fe);
	
	pcout
		<< "ewave_dG: distributed dofs"
		<< std::endl;
	
#ifdef DoFRenumbering_component_wise
	dealii::DoFRenumbering::component_wise(*dof);
	
	pcout
		<< "ewave_dG: dealii::DoFRenumbering::component_wise(*dof);"
		<< std::endl;
#endif
}


template<int dim>
void UnitTest<dim>::
make_partitioning ()
{
	AssertThrow(dof.use_count(), dealii::ExcNotInitialized());
	
	// get all dofs componentwise
	std::vector< dealii::types::global_dof_index > dofs_per_component(
		dof->get_fe_collection().n_components(), 0
	);
	dealii::DoFTools::count_dofs_per_component(*dof, dofs_per_component, true);
	
	// set specific values of dof counts
	dealii::types::global_dof_index N_u = 0;
	for (unsigned int d{0}; d < dim; ++d) {
		N_u += dofs_per_component[0*dim+d]; // Number of DoF of the displacement field
	}
	
	// set specific global dof offset values
	dealii::types::global_dof_index N_u_offset = 0;
	
	pcout << "ewave_dG: distribute_dofs overview: " << std::endl;
	pcout << "\tN_u = " << N_u << std::endl;
	pcout << "\tN_u_offset = " << N_u_offset << std::endl;
	pcout << "\tn_dofs = " << dof->n_dofs() << std::endl;
	
	// create distributed-parallel dof partitionings 
	locally_owned_dofs = std::make_shared< dealii::IndexSet >();
	*locally_owned_dofs = dof->locally_owned_dofs();
	
	locally_relevant_dofs = std::make_shared< dealii::IndexSet >();
	dealii::DoFTools::extract_locally_relevant_dofs(*dof, *locally_relevant_dofs);
	
	partitioning_locally_owned_dofs =
		std::make_shared< std::vector< dealii::IndexSet > >();
	partitioning_locally_owned_dofs->push_back(
		locally_owned_dofs->get_view(N_u_offset, N_u_offset+N_u)
	);
	
	partitioning_locally_relevant_dofs =
		std::make_shared< std::vector< dealii::IndexSet > >();
	partitioning_locally_relevant_dofs->push_back(
		locally_relevant_dofs->get_view(N_u_offset, N_u_offset+N_u)
	);
}


template<int dim>
void UnitTest<dim>::
make_sparsity_pattern ()
{
	////////////////////////////////////////////////////////////////////////////
	// Operator A (IPDG) basis function couplings
	dealii::Table<2,dealii::DoFTools::Coupling> coupling_block_A_K(
		dim, dim
	);
	dealii::Table<2,dealii::DoFTools::Coupling> coupling_block_A_F(
		dim, dim
	);
	{
		// loop 0: init with non-coupling
		for (unsigned int i{0}; i < dim; ++i)
		for (unsigned int j{0}; j < dim; ++j) {
			coupling_block_A_K[i][j] = dealii::DoFTools::none;
			coupling_block_A_F[i][j] = dealii::DoFTools::none;
		}
		// loop 1a: init with coupling for chi-chi (matrix A_uu)
		for (unsigned int i{0}; i < dim; ++i)
		for (unsigned int j{0}; j < dim; ++j) {
			coupling_block_A_K[i][j] = dealii::DoFTools::always;
			coupling_block_A_F[i][j] = dealii::DoFTools::nonzero;
		}
	}
	
	// biot: "A" block matrix (IPDG)
	auto sp_block_A =
		std::make_shared< dealii::TrilinosWrappers::BlockSparsityPattern > ();
	
	AssertThrow(
		partitioning_locally_owned_dofs.use_count(),
		dealii::ExcNotInitialized()
	);
	
	sp_block_A->reinit(
		*partitioning_locally_owned_dofs,
		mpi_comm
	);
	
	dealii::DoFTools::make_flux_sparsity_pattern(
		*dof,
		*sp_block_A,
		coupling_block_A_K, coupling_block_A_F,
		dealii::Utilities::MPI::this_mpi_process(mpi_comm)
	);
	
	sp_block_A->compress();
	
	sp_A =
		std::make_shared< dealii::TrilinosWrappers::SparsityPattern > ();
	
	sp_A->copy_from(sp_block_A->block(0,0));
	
	// debug output of the distributed SparsityPattern
	// through all processes
	// can be plotted with plotdrop sp_A* (gnuplot)
// #ifdef DEBUG
	const unsigned int MyPID{
		dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)
	};
	
	{
		std::ostringstream filename;
		filename << "sp_A_" << MyPID << ".gpl";
		std::ofstream outM(filename.str().c_str(), std::ios_base::out);
		
		sp_A->print_gnuplot(outM);
	}
// #endif
	
	pcout
		<< "ewave_dG: created SparsityPattern"
		<< std::endl;
}


////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[]) {
	// Init MPI (or MPI+X)
	dealii::Utilities::MPI::MPI_InitFinalize mpi(argc, argv, MPIX_THREADS);
	
	try {
// 		dealii::deallog.depth_console(0);
		
		auto problem = std::make_shared<UnitTest<2>> ();
		
		problem->run();
		
	}
	catch (std::exception &exc) {
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An EXCEPTION occured: Please READ the following output CAREFULLY!"
						<< std::endl;
			
			std::cerr	<< exc.what() << std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		return 1;
	}
	catch (...) {
		if (!dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
			std::cerr	<< std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl
						<< "An UNKNOWN EXCEPTION occured!"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl << std::endl
						<< "Further information:" << std::endl
						<< "\tThe main() function catched an exception"
						<< std::endl
						<< "\twhich is not inherited from std::exception."
						<< std::endl
						<< "\tYou have probably called 'throw' somewhere,"
						<< std::endl
						<< "\tif you do not have done this, please contact the authors!"
						<< std::endl << std::endl
						<< "----------------------------------------"
						<< "----------------------------------------"
						<< std::endl;
			
			std::cerr	<< std::endl
						<< "APPLICATION TERMINATED unexpectedly due to an exception."
						<< std::endl << std::endl
						<< "****************************************"
						<< "****************************************"
						<< std::endl << std::endl;
		}
		
		return 1;
	}
	
	return 0;
}
